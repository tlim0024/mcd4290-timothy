//to check the code, uncomment the corresponding question.

question1();
question2();
question3();
question4();
question5();

function question1() {
    let output = "" //empty output, fill this so that it can print onto the page.
    
    //Question 1 here 
    let dataArray = [54, -16, 80, 55, -74, 73, 26, 5, -34, -73, 19, 63, -55, -61, 65, -14, -19, -51, -17, -25];
    let posOdd=[];
    let negEven=[];
    
    console.log(dataArray[0])
    for (let i = 0; i < dataArray.length; i++) {
        if (dataArray[i]>0 && dataArray[i]%2 != 0) {
            posOdd.push(dataArray[i])
        }
        else if (dataArray[i]<0 && dataArray[i]%2 == 0) {
            negEven.push(dataArray[i])
        }
        else {
        console.log("Cannot");
        }
    }

    output += "Positive: " + "\n";
    output += posOdd + "\n";
    output += "Negative: " + "\n";
    output += negEven;
    
         
    let outPutArea = document.getElementById("outputArea1") //this line will find the element on the page called "outputArea1"
    outPutArea.innerText = output //this line will fill the above element with your output.
}


function question2() {
    let output = "" 
    
    //Question 2 here
    // 6 variables
    //60000
    let random;
    let face;
    let face1 = 0;
    let face2 = 0;
    let face3 = 0;
    let face4 = 0;
    let face5 = 0;
    let face6 = 0;
    
    for (let i = 0; i < 60000; i++) {
        random = Math.floor((Math.random()*6)+1)
                  
        if (random === 1) {
            face1++;
        }
        else if (random === 2) {
            face2++;
        }
        else if (random === 3) {
            face3++;
        }
        else if (random === 4) {
            face4++;
        }
        else if (random === 5) {
            face5++;
        }
        else if (random === 6) {
            face6++;
        }
    }


    let i = 0;
    output += "Frequency of die rolls" + "\n";
    output += '1: ' + face1 + '\n';
    output += '2: ' + face2 + '\n';
    output += '3: ' + face3 + '\n';
    output += '4: ' + face4 + '\n';
    output += '5: ' + face5 + '\n';
    output += '6: ' + face6 + '\n';
    
    
    let outPutArea = document.getElementById("outputArea2") 
    outPutArea.innerText = output 

}

function question3() {
    let output = "" 
    
    //Question 3 here 
    //Use a 7 element array
    //let frequency=[0, 0, 0, 0, 0, 0, 0];
    //frequency = [6];
    let random;
    var arr = [0,0,0,0,0,0,0];

    for(var i = 0; i < 60000; i++) {
        random = Math.floor((Math.random()*6)+1);
      arr[random]++; 
    }

    output += "Frequency of die rolls" + "\n";
    for (let i = 1; i <= 6; i++) {
        output += i +": " + (arr[i]) + "\n";
    }
  
    
    let outPutArea = document.getElementById("outputArea3")
    outPutArea.innerText = output 
    }


function question4() {
    let output = "" 
    
    //Question 4 here

    var diceRolls = {
        Frequency: {
                1:0,
                2:0,
                3:0,
                4:0,
                5:0,
                6:0,    
            },
            Total: 60000,
            Exceptions: "" 
    }
    
    //Variables
    let randDice = 0;
    
    for (let i = 0; i < diceRolls.Total; i++) {
        randDice = Math.floor((Math.random()*6)+1);
        
        diceRolls.Frequency[randDice]++;
    }
    
    output += "Frequency of dice rolls\n";
    output += "Total rolls: " + diceRolls.Total + '\n';
    output += "Frequencies: "
    
    for (let k = 1; k < 7; k++) {
        output += '\n'+ k + ": " + diceRolls.Frequency[k];
        
        if (diceRolls.Frequency[k] < 9900) {
            diceRolls.Exceptions += '\t' + diceRolls.Frequency[k];
        }
        else if (diceRolls.Frequency[k] > 10100) {
            diceRolls.Exceptions += '\t'+ diceRolls.Frequency[k];
        }
    }
    
    output += '\nExceptions: ' + diceRolls.Exceptions
 
    
    let outPutArea = document.getElementById("outputArea4") 
    outPutArea.innerText = output 
}

function question5() {
    let output = "" 
    
    //Question 5 here
    // if.. else..
    let person = {
      name: "Jane",
    income: 127050
    }
    
    let x = 127050;
    x === person.income;
    let payTax = 0;
    let tax = 0;
    
    
    if (x === 0 && x <= 18200) {
        payTax = x;
    }
    else if (x >= 18201 && x <= 37000) {
        tax = x - 18200;
        payTax = (0.19 * tax) // 1$ = 100c
    }
    else if (x >= 37001 && x <= 90000) {
        tax = x - 37000;
        payTax = 3572 + (0.325 * tax)
    }
    else if (x >= 90001 && x <= 180000) {
        tax = x - 90000;
        payTax = 20797 + (0.37 * tax)
    }
    else if (x >= 180001) {
        tax = x - 180000;
        payTax = 54097 + (0.45 * tax)
    }
    
    
    output += "Jane\'s income is: $" + person.income + ", and her tax owed is: $" + payTax
    
    let outPutArea = document.getElementById("outputArea5") 
    outPutArea.innerText = output 
}