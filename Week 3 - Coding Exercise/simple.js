question1();
question2();
question3();
question4();



/*Question 1
Code a function called objectToHTML that returns a nicely formatted String suitable for rendering as HTML listing all of the properties and property values of an object supplied as a parameter to the function.
Code a test object with several properties to test your function. Display the results of your testing. For example, running your code on the following object*/

//use a function
//called objectToHTML
//returns a nicely formatted String
function question1() {
let output="";

var testObj = { 
    number: 1,
    string: "abc",
    array: [5, 4, 3, 2, 1],
    boolean: true
};

//test the function
//console.log(objectToHTML());
output+= objectToHTML(testObj);
document.getElementById("outputArea1").innerHTML=output;

function objectToHTML(number, string, array, boolean) {
    let retStr="";
    number = retStr += "number: " + testObj.number + "</br>";
    string = retStr += "string: " + testObj.string + "</br>";
    array = retStr += "array: " + testObj.array + "</br>";
    boolean = retStr += "boolean: " + testObj.boolean + "</br>";
    
    return number, string, array, boolean;
    }
}




/*Question 2
Check the output of the following code then recode it to eliminate functions defined in-place as parameters.
*/


function question2() {

    let outputAreaRef = document.getElementById("outputArea2"); 
    let output = "";

    function flexible(fOperation, operand1, operand2) {
        let result = fOperation(operand1, operand2);
        return result;
    }

    function sum(a, b) {
        return a + b;
    }

    output += flexible(sum, 3, 5) + "<br/>";
    output += flexible(function (num1, num2) {return num1 * num2}, 3, 5) + "<br/>";
    outputAreaRef.innerHTML = output;

}




/*Question 3
Write pseudocode for a single function called extremeValues. It takes 1 parameter, an array of integers (you do not need to validate this). The function should return the minimum AND maximum values in the array.
The standard approach for finding a minimum value in an array is to assume the first element (at index 0) is the current minimum. Then process the array repetitively from its second element to its last. On each iteration compare the current element being processed with the current minimum. If it’s less than the minimum then set it as the current minimum. In this way at the end of processing the current minimum holds the minimum element value in the array. A similar process will work for finding the maximum.*/


function question3() {
    
    let outputAreaRef = document.getElementById("outputArea3"); 
    let output = "";
    
    output += "make a " +  "function extremeValues(){} " +  "<br/>";
    output += "let nums = [4,3,6,12,1,3,8]; " + "<br/>";
    output += "let max = math.max " + "<br/>";
    output += "let min = math.min " + "<br/>";
    outputAreaRef.innerHTML = output;
    
}





/*Question 4
Using your pseudocode from the previous task, now code your function so that it prints to the output area, the smallest and largest elements in the array.
*/


function question4() {
    
    let outputAreaRef = document.getElementById("outputArea4"); 
    let output = "";
    
    let a = Math.max(4, 3, 6, 12, 1, 3, 8);
    let b = Math.min(4, 3, 6, 12, 1, 3, 8);
    
    function extremeValues() {
        return a, b;
    }
    
    
    output += "Max value: " + console.log(a) + "<br/>";//will appear in console
    output += "Min value: " + console.log(b) + "<br/>";//will appear in console
    outputAreaRef.innerHTML = output;
    
}