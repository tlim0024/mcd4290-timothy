/*Using only the two literal Strings:
let animalString = "cameldogcatlizard";
let andString = " and ";
Use both String methods substr(…) and substring(…) to output “cat and dog”.
Look up documentation (w3schools or Mozilla) to understand the different semantics of the
parameters of these 2 methods. Be prepared to show your demonstrator the documentation you
used and to explain the difference between substr(…) and substring(…).*/

let animalString = "cameldogcatlizard";
let andString = " and ";

let cat = animalString.substring(11,8);
//console.log(cat);
let dog = animalString.substr(5,6);
let dog2 = dog.substr(0,3);
//console.log(dog2);

let combine = cat + andString + dog2;
console.log(combine);
