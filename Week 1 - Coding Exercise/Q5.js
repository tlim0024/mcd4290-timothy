/*In the following code what is wrong with the Boolean expression?
• Try plugging in different values of year and check the output.
• Fix the problem?
• Make sure you understand the operator precedence order especially with respect to
assignment, relationals and logicals.*/

let year;
let yearNot2015Or2016;
year = 2000; //change year to 2015 and 2016
yearNot2015Or2016 = year !== 2015 && year !== 2015;
console.log(yearNot2015Or2016);


