/*Given the following information about a person:
First Name = Kanye
Last Name = West
Birth Date = 8 June 1977
Annual Income = 150000000.00
Write some code to create an Object with the information above as properties with the appropriate
data type for each property. Once you have created this object, print out the details to the console*/

var person = {
    firstName:"Kanye",
    lastName:" West",
    birthDate:" 8 June 1977",
    annualIncome:" $150000000.00"
};

var result = person.firstName + person.lastName + " was born on" + person.birthDate + 
    " and has an annual income of" + person.annualIncome;
console.log(result);