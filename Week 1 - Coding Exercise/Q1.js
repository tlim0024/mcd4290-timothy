/*Given the following circle with a radius of r = 4, write code to find the circumference of this circle c
and print the result to console to 2 decimal places.
Use the Math.PI and toFixed methods to help you with this.*/

let r = 4;
let pi = Math.PI;   
let C = 2 * pi * r; 
let result = C.toFixed(2);  
console.log(result); 